export interface Attack {
  readonly name: string
  readonly type: string
  readonly damage: number
}


export interface Pokemon {
  readonly id: string
  readonly number: number
  readonly name: string
  readonly weight: PokemonDimension
  readonly height: PokemonDimension
  readonly classification: string
  readonly types: string[]
  readonly resistant: string[]
  readonly attacks: PokemonAttack
  readonly weaknesses: string[]
  readonly fleeRate: number
  readonly maxCP: number
  readonly evolutions: Pokemon[]
  readonly maxHP: number
  readonly image: string
  readonly sound: string
  readonly isFavorite: boolean
  readonly evolutionRequirements?: PokemonEvolutionRequirement
}

export type PokemonShort = Pick<Pokemon, "id" | "name" | "types" | "isFavorite" | "image">

export interface  PokemonAttack {
  readonly fast: Attack[]
  readonly special: Attack[]
}

export interface PokemonConnection {
  readonly limit: number
  readonly offset: number
  readonly count: number
  readonly edges: Pokemon[]
}

export interface PokemonDimension {
  readonly minimum: string
  readonly maximum: string
}

export interface PokemonEvolutionRequirement {
  readonly amount: number
  readonly name: string
}

// Taken from https://gist.github.com/apaleslimghost/0d25ec801ca4fc43317bcff298af43c3
// absolute blast
export const PokemonTypeColorMap = new Map(Object.entries({
	Normal: '#A8A77A',
	Fire: '#EE8130',
	Water: '#6390F0',
	Electric: '#F7D02C',
	Grass: '#7AC74C',
	Ice: '#96D9D6',
	Fighting: '#C22E28',
	Poison: '#A33EA1',
	Ground: '#E2BF65',
	Flying: '#A98FF3',
	Psychic: '#F95587',
	Bug: '#A6B91A',
	Rock: '#B6A136',
	Ghost: '#735797',
	Dragon: '#6F35FC',
	Dark: '#705746',
	Steel: '#B7B7CE',
	Fairy: '#D685AD',
}));
