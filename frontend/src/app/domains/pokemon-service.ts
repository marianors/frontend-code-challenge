import { PokemonShort } from "./pokemon"

export interface PokemonListQueryResponse {
    readonly pokemons: {
      readonly edges: PokemonShort[]
    }
}

export interface PokemonCountQueryResponse {
  readonly pokemons: {
    readonly count: number;
  }
}

// According to wiki
export const POKEMON_COUNT = 1025;