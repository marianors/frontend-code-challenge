import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { PokemonShort } from '../domains/pokemon';
import { Apollo, gql } from 'apollo-angular';
import { POKEMON_COUNT, PokemonCountQueryResponse, PokemonListQueryResponse } from '../domains/pokemon-service';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  constructor(private readonly apollo: Apollo) { }

  private getSelectQuery(limit: number = 10, offset: number = 0, favoritesOnly: boolean = false, search?: string, type?: string): string {
    // Normalize for gql
    search = search ? `"${search}"` : "null";
    type = type ? `"${type}"` : "null";
     
    return `
     pokemons(query: {
      limit: ${limit},
      offset: ${offset},
      search: ${search},
      filter: {
        isFavorite: ${favoritesOnly},
        type: ${type}
      } })
      ` 
  }

  getList(limit: number = 10, offset: number = 0, favoritesOnly: boolean = false, search?: string, type?: string): Observable<PokemonShort[]> {

      const query = gql`
      { ${this.getSelectQuery(limit, offset, favoritesOnly, search, type)} { edges { name, id, isFavorite, image, types, } } }
      `
      return this.apollo.query<PokemonListQueryResponse>({query}).pipe(map((resp) => {
        return resp.data ? resp.data.pokemons.edges as PokemonShort[] : [];
      }))
  }

  getCount(favoritesOnly: boolean = false, search?: string, type?: string): Observable<number> {
    const query = gql`
    { ${this.getSelectQuery(-1, 0, favoritesOnly, search, type)} { count } }
    `
    return this.apollo.query<PokemonCountQueryResponse>({query}).pipe(map((resp) => {
      return resp.data ? resp.data.pokemons.count : 0;
    }))
}
  getAllNames(): Observable<string[]> {
    const query = gql`
      { pokemons(query: { limit: -1 } ) { edges { name } } }
      `
    return this.apollo.query<{pokemons: {edges: {name: string}[]} }>({query}).pipe(map((resp) => {
      return resp.data ? resp.data.pokemons.edges.map((edge) => edge.name) : [];
    }))
  }

  getAllTypes(): Observable<string[]> {
    const query = gql`
    { pokemonTypes }
    `
    return this.apollo.query<{pokemonTypes: string[]}>({query}).pipe(map((resp) => {
      return resp.data ? resp.data.pokemonTypes : [];
    }))
  }

  favoritePokemon(id: string): Observable<PokemonShort | undefined> {
    const mutation = gql`
    mutation { favoritePokemon(id: "${id}") { name, id, isFavorite, image, types } }
    `
    return this.apollo.mutate<{favoritePokemon: PokemonShort}>({mutation}).pipe(map((resp) => {
      return resp.data?.favoritePokemon;
    }))
  }

  unfavoritePokemon(id: string): Observable<PokemonShort | undefined> {
    const mutation = gql`
    mutation { unFavoritePokemon(id: "${id}") { name, id, isFavorite, image, types } }
    `
    return this.apollo.mutate<{unFavoritePokemon: PokemonShort}>({mutation}).pipe(map((resp) => {
      return resp.data?.unFavoritePokemon;
    }))
  }
}
