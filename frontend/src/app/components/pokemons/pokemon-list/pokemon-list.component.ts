import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PokemonShort, PokemonTypeColorMap } from '../../../domains/pokemon';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrl: './pokemon-list.component.scss'
})
export class PokemonListComponent {
  @Input() pokemons: PokemonShort[] = [];

  @Output() favorited = new EventEmitter<string>();
  @Output() unfavorited = new EventEmitter<string>();

  getTypeGradient(pokemon: PokemonShort): string {
    // According to Wiki, it's either 1 or 2, could be updated in future
    if (pokemon.types.length == 1) {
      return PokemonTypeColorMap.get(pokemon.types[0]) || "var(--bs-card-cap-bg)";
    }
    return `linear-gradient(90deg, ${PokemonTypeColorMap.get(pokemon.types[0])}, ${PokemonTypeColorMap.get(pokemon.types[1])})`
  }

  toggleFavorite(pokemon: PokemonShort) {
    if (pokemon.isFavorite) {
      this.unfavorited.emit(pokemon.id);
    } else {
      this.favorited.emit(pokemon.id);
    }
  }
}
