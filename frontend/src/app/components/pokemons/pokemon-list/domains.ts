export interface PaginationOptions {
  readonly pageSize: number
  readonly offset: number
}
