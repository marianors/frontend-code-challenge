import { Component, OnDestroy, OnInit } from '@angular/core';
import { PokemonService } from '../../../services/pokemon.service';
import { PokemonShort } from '../../../domains/pokemon';

import { BehaviorSubject, Subject, combineLatest, debounceTime, distinctUntilChanged, filter, skipWhile, takeUntil, tap, zip } from 'rxjs';
import { PaginationOptions } from './domains';
import { FilterOptions } from './pokemon-list-filters/domains';
import { ToastrService } from 'ngx-toastr';

const PAGE_SIZES = [8, 16, 32, 64];
const DEFAULT_PAGINATION = {
  pageSize: PAGE_SIZES[0],
  offset: 0,
}
const DEBOUNCE_TIME = 200;
const DEFAULT_FILTERS: FilterOptions = {
  favoritesOnly: false
}


@Component({
  selector: 'app-dynamic-pokemon-list',
  styles: [".page-size { width: auto }"],
  template: `
    <div class="container mt-5 text-center">
    @if (initLoading$ | async) {
      <div class="spinner-border" role="status">
        <span class="sr-only"></span>
      </div>
    } @else {
      <app-pokemon-list-filters
        [types]="(pokemonTypes$ | async)!"
        [pokemonNames]="(pokemonNames$ | async)!"
        (filtersUpdated)="handleFilterUpdate($event)"
      ></app-pokemon-list-filters>
      
      @if (loading$ | async) {
        <div class="spinner-border" role="status">
          <span class="sr-only"></span>
        </div>
      } @else {
        <app-pokemon-list
          [pokemons]="(pokemons$ | async)!"
          (favorited)="favoritePokemon($event)"
          (unfavorited)="unfavoritePokemon($event)"
        ></app-pokemon-list>

        <div class="pagination-controls row">
          <div class="col-2">
            <select
              #sizeSelect
              class="page-size form-select pull-right"
              aria-label="Page size select"
              (change)="updatePageSize(sizeSelect.value)"
              >
              @for (size of pageSizes; track size) {
                <option>{{ size }}</option>
              }
            </select>
          </div>
          <div class="col-8">
            <ngb-pagination
              class="d-flex justify-content-center"
              [maxSize]="5"
	            [rotate]="true"
              [(page)]="page"
              [pageSize]="(paginationOptions$ | async)!.pageSize"
              [collectionSize]="(filteredCount$ | async)!" 
              (pageChange)="updatePage(page)"
              />
          </div>
         
        </div>
      }
    }
  `
})
export class DynamicPokemonListComponent implements OnInit, OnDestroy {
  private readonly _pokemons = new BehaviorSubject<PokemonShort[]>([]) ;
  readonly pokemons$ = this._pokemons.pipe();

  private readonly _pokemonTypes = new BehaviorSubject<string[]>([]) ;
  readonly pokemonTypes$ = this._pokemonTypes.pipe();

  private readonly _pokemonNames = new BehaviorSubject<string[]>([]) ;
  readonly pokemonNames$ = this._pokemonNames.pipe();

  private readonly _filteredCount = new BehaviorSubject<number>(0) ;
  readonly filteredCount$ = this._filteredCount.pipe();

  private readonly _initLoading = new BehaviorSubject<boolean>(false) ;
  readonly initLoading$ = this._initLoading.pipe();

  private readonly _loading = new BehaviorSubject<boolean>(false) ;
  readonly loading$ = this._loading.pipe();

  private readonly _loadError = new BehaviorSubject<string | undefined>(undefined) ;
  readonly loadError$ = this._loadError.pipe();

  private readonly _filters = new BehaviorSubject<FilterOptions>(DEFAULT_FILTERS) ;

  private readonly _paginationOptions = new BehaviorSubject<PaginationOptions>({...DEFAULT_PAGINATION}) ;
  readonly paginationOptions$ = this._paginationOptions.pipe();

  private readonly destroy = new Subject();
  
  readonly pageSizes = PAGE_SIZES;
  page: number = 0;

  constructor(private readonly pokemonService: PokemonService, private readonly toastr: ToastrService) {
  
  }

  ngOnInit(): void {
    this._initLoading.next(true);

    zip(this.pokemonService.getAllTypes(),  this.pokemonService.getAllNames( ))
    .subscribe({
      next: ([types, names]) => {
        this._pokemonTypes.next(types);
        this._pokemonNames.next(names);
        this._filteredCount.next(names.length);
      },
      error: (err) => {
        this._loadError.next(JSON.stringify(err))
      }
      
    }).add(() => {
      this._initLoading.next(false);
      this._loading.next(true);

      const filters$ = this._filters.pipe(
        takeUntil(this.destroy),
        debounceTime(DEBOUNCE_TIME),
        skipWhile((v) => !v),
        distinctUntilChanged(),
        tap((filters) => {
          this.page = 0;
          this.pokemonService.getCount(filters!.favoritesOnly, filters!.name, filters!.type)
          .subscribe((cnt) => {this._filteredCount.next(cnt)})
        })
      )
      const pagination$ = this._paginationOptions.pipe(
        takeUntil(this.destroy),
        debounceTime(DEBOUNCE_TIME),
        skipWhile((v) => !v),
        distinctUntilChanged(),
      )
      
      combineLatest([filters$, pagination$])
        .subscribe(([filters, pagination]) => {
          this._loading.next(true);
          this.pokemonService.getList(
            pagination.pageSize,
            pagination.offset * pagination.pageSize,
            filters!.favoritesOnly,
            filters!.name,
            filters!.type
          )
          .subscribe((pokemons) => {this._pokemons.next(pokemons)})
          .add(() => this._loading.next(false));
        })
    })
  }

  favoritePokemon(pokemonId: string): void {
    this.pokemonService.favoritePokemon(pokemonId).subscribe({
      next: (pokemon) => {
        this.toastr.success(`${pokemon?.name} added to favorites.`);
        this.updatePokemon(pokemon!);
      },
      error: () => {
        this.toastr.error("Adding to favorites failed");
      }
    })
  }

  unfavoritePokemon(pokemonId: string): void {
    this.pokemonService.unfavoritePokemon(pokemonId).subscribe({
      next: (pokemon) => {
        this.toastr.success(`${pokemon?.name} removed from favorites.`);
        this.updatePokemon(pokemon!);
      },
      error: () => {
        this.toastr.error("Removing favorites failed");
      }
    })
  }

  
  handleFilterUpdate(filters: FilterOptions): void {
    this._filters.next(filters);
  }

  updatePage(page: number) {
    this._paginationOptions.next({...this._paginationOptions.getValue(), offset: page - 1})
  }

  updatePageSize(size: string) {
    this._paginationOptions.next({...this._paginationOptions.getValue(), pageSize: Number(size)})
  }

  ngOnDestroy(): void {
      this.destroy.next(true);
      this.destroy.complete();
  }

  private updatePokemon(pokemon: PokemonShort) {
    // Cheekily replace a pokemon in the list without re-fetching the data, or re-rendering everything
    this._pokemons.next(this._pokemons.getValue().map((pokemon_) => pokemon_.id == pokemon.id ? pokemon : pokemon_))
  }
}
