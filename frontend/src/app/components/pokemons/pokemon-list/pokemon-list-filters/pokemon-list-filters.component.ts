import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FilterOptions } from './domains';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, OperatorFunction, Subject, debounceTime, distinctUntilChanged, map, takeUntil, tap } from 'rxjs';

@Component({
  selector: 'app-pokemon-list-filters',
  templateUrl: './pokemon-list-filters.component.html',
  styleUrl: './pokemon-list-filters.component.scss'
})
export class PokemonListFiltersComponent implements OnInit, OnDestroy {
  @Input() types: string[] = [];
  @Input() pokemonNames: string[] = [];
  @Output() filtersUpdated = new EventEmitter<FilterOptions>;

  private readonly destroy = new Subject();

  filterParameters = new FormGroup({
    type: new FormControl<string | undefined>(""),
    name: new FormControl<string | undefined>(undefined),
    favoritesOnly: new FormControl<boolean>(false),
  });

  searchName: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(100),
      distinctUntilChanged(),
      map((term) =>
        term.length < 1 ? [] : this.pokemonNames.filter((v) => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10),
      ),
    );

  ngOnInit(): void {
    this.filterParameters.valueChanges.pipe(takeUntil(this.destroy), tap(console.log)).subscribe((filters) => this.filtersUpdated.emit({
      name: filters.name!, 
      type: filters.type!,
      favoritesOnly: Boolean(filters.favoritesOnly),
    }))
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.complete();
  }
}