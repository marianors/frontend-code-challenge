export interface FilterOptions {
  readonly favoritesOnly: boolean
  readonly name?: string
  readonly type?: string
}