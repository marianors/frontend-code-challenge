import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-dynamic-pokemon-detail',
  styleUrl: './pokemon-detail.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-pokemon-detail></app-pokemon-detail>
  `
})
export class DynamicPokemonDetailComponent {

}
