import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicPokemonDetailComponent, PokemonDetailComponent } from './pokemon-detail';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { DynamicPokemonListComponent } from './pokemon-list';
import { PokemonListFiltersComponent } from './pokemon-list/pokemon-list-filters/pokemon-list-filters.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbPagination, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [DynamicPokemonDetailComponent, PokemonDetailComponent, PokemonListComponent, DynamicPokemonListComponent, PokemonListFiltersComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTypeaheadModule,
    NgbPagination,
  ],
  exports: [DynamicPokemonDetailComponent, PokemonDetailComponent, PokemonListComponent, DynamicPokemonListComponent],
})
export class PokemonsModule { }
