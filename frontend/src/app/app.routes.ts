import { Routes } from '@angular/router';
import { DynamicPokemonDetailComponent } from './components/pokemons/pokemon-detail';
import { DynamicPokemonListComponent } from './components/pokemons/pokemon-list';


export const routes: Routes = [
  { path: "", component: DynamicPokemonListComponent },
  { path: ":name", component: DynamicPokemonDetailComponent },
];
